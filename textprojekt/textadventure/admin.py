from django.contrib import admin
from . import models

class ChoiceInline(admin.TabularInline):
    model = models.Choice
    fk_name = 'station'
    extra = 1

class StationAdmin(admin.ModelAdmin):
    inlines = [ChoiceInline, ]
    list_display = ['name', 'text', "get_choices"]
    def get_choices(self, obj):
        return " / ".join([str(o) for o in obj.choice_set.all()])
    get_choices.short_description = "choices"        

admin.site.register(models.Station, StationAdmin)    