from django.urls import path
from . import views

urlpatterns = [
        # Default, Station mit dem Namen "start"
        path('', views.station, name="station-default"),
        path('<str:name>', views.station, name="station")
]
